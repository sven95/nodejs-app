var express = require('express');
var router = express.Router();
// File reader
var reader= require(__dirname + '/../helpers/crud/read/read.js');
// Update funcions
var update= require(__dirname + '/../helpers/crud/update/update.js')
var fs=require('fs');

/* GET home page. */
router.get('/', function(req, res, next) {
  reader.readStorage(function(arrayOfObjects){
    res.render('index', { title: 'NodeJS app' ,data: arrayOfObjects.presenter});
  });
});

/* POST home page.
   Reads the storage. Rewrites the array with new information. Rewrites the file in Json format.
   The page is then reloaded.
*/
router.post('/',function(req,res,next){
  newData= req.body;
  var data= "Data not found";

  reader.readStorage(function(arrayOfObjects){
    update.jsonArray(arrayOfObjects, newData, function(newJsonFile){
      update.jsonFile(newJsonFile,function(newJsonFile){
        res.render('index', { title: 'NodeJS app' ,data: newJsonFile.presenter});
      });
    });
  });

});


module.exports = router;
