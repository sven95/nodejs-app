var request = require('supertest');
var app = require("../app");


describe('Loading express', function () {
  var server;
  beforeEach(function () {
    server = require("../app.js");
  });
  it('responds to /', function testPage(done) {
  request(server)
    .get('/')
    .expect(200, done);
  });
  it('404 everything else', function testPath(done) {
    request(server)
      .get('/foo/bar')
      .expect(404, done);
  });
});


