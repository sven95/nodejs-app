var fs=require('fs');
// Function that returns the contents of the Json file.
function readStorage(callback){
	fs.readFile(__dirname + '/../../../storage/presenters.json', 'utf-8', function(err, data) {
		if (err) throw err;
		var arrayOfObjects = JSON.parse(data);
		callback(arrayOfObjects);
	});
};
module.exports = {readStorage};
