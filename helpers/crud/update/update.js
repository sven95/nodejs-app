var fs=require('fs');
var validator=require('validator');

// Rewrites the JSON file with new information provided with the newJsonFile parameter.
function jsonFile(newJsonFile,callback){
  fs.writeFile(__dirname + '/../../../storage/presenters.json', JSON.stringify(newJsonFile), 'utf-8', function(err) {
  if (err) throw err
  console.log(err);
  })
  callback(newJsonFile);
}

// The function rewrites the JSON array, with the values provided through the POST method.
function jsonArray(arrayOfObjects, newData, callback)
  { var i=0;
    for(var presenter in arrayOfObjects.presenter)
    {
      arrayOfObjects.presenter[i].name= escape(newData.name[i]);
      arrayOfObjects.presenter[i].surname= escape(newData.surname[i]);
      i++;
    }
    callback(arrayOfObjects);
}

module.exports = {jsonFile, jsonArray};
