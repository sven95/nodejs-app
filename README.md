# README #

Instructions

    Start the application by running "npm start"
    Access http://localhost:3000
    See and modify the contents of the Json file which can be found in "/storage/presenters.json"
	
Application

    This application has been created to demonstrate building web applications using NodeJS. At the same time, I have opted for creating a local file storage system
	using Json files. SQL technologies can be used, yet by doing this I believe a more in depth understanding of data can be shown.

Choices

    I decided to use NodeJS, as to me it seems more lightweight than rails.
    A Json file is used for storage, as information is easily accessible.
    The fs node module was used for reading and writing onto the Json file.
    'Validator' has been used to escape user input.
    The code has been separated for better visualisation and to avoid repetition.
    Testing has been done manually. An automated test can be found in the "/test" file. I am no familiar with testing in NodeJS, apologies for lacking in this section.

